def cycles_time(cycle):
    return cycle / 2.6e9

def time_cycle(time):
    return time * 2.6e9

def get_text(filename):
    with open(filename) as f:
        data = f.read()
    return data

def array_csv(array, filename):
    f = open(filename, 'w')
    for e in array:
        f.write(e[0] + ',', e[1])
    f.close()
    return 0
