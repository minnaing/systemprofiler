#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "../include/rdtsc.c"

size_t getFileSize(int fd) {
    struct stat buf;
    fstat(fd, &buf);
    return buf.st_size;
}

int main(int argc, const char * argv[]) {

    int BLOCKSIZE = 4 * 1024;
    void* buf = malloc(BLOCKSIZE);
    int readByte = 0;
	
    int fd = open(argv[1], O_RDONLY | O_SYNC);
    int FILESIZE = getFileSize(fd);

    lseek(fd, FILESIZE - 1, SEEK_SET);
    while(1){
        lseek(fd, -2*BLOCKSIZE, SEEK_CUR);
        int x = read(fd, buf, BLOCKSIZE);
        if ( x <= 0) { break; }
        readByte += x;
        if (readByte >= FILESIZE) { break; }
    }
    close(fd);
 
    long start, end, total;
    total = 0;
    readByte = 0;

    // --- [ Second time reading ] ---
    fd = open(argv[1], O_RDONLY| O_SYNC);
    
    lseek(fd, FILESIZE - 1, SEEK_SET);
    while(1){
        lseek(fd, -2*BLOCKSIZE, SEEK_CUR);
        start = rdtsc();
        int x = read(fd, buf, BLOCKSIZE);
        end = rdtsc();
        total += end - start;
        if (x <= 0){ break; }  
        readByte += x;
        if (readByte >= FILESIZE) { break; }      
    }
    
    close(fd);
    free(buf);
    double result = (total / (FILESIZE / BLOCKSIZE)) /  (2.6* 1000);
    printf(" Access time : %lf\n", result);
    return 0;
}
