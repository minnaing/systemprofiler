#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include "../include/rdtsc.c"

const off_t BLOCKSIZE = 4 * 1024;
int fileindex = 0;

size_t getFileSize(int fd) {
    struct stat buf;
    fstat(fd, &buf);
    return buf.st_size;
}

int *proc_create(int n) {
    int *childs = malloc(sizeof *childs * n);
    
    for (int i = 0; i < n; i++) {
        int pid = fork();
        if (pid < 0) {
            perror("Fork Failed!");
            exit(1);
        } else if (pid == 0) { // Child
            return NULL;
        } else { // Parent
            childs[i] = pid;
            fileindex = i + 1;
        }
    }
    return childs;
}

int main(int argc, char* argv[]) {
    int nProcess = 8;
    int TIMES = 10;
    
    char* filenames[8];
    filenames[0] = "../temp/32M.0";
    filenames[1] = "../temp/32M.1";
    filenames[2] = "../temp/32M.2";
    filenames[3] = "../temp/32M.3";
    filenames[4] = "../temp/32M.4";
    filenames[5] = "../temp/32M.5";
    filenames[6] = "../temp/32M.6";
    filenames[7] = "../temp/32M.7";
    
    
    int *childs = proc_create(nProcess);
    int nBlocks;
    
    long start, end, total;
    total = 0;
    
    long start_1, end_1, total_1;
    total_1 = 0;
    
    for (int i = 0; i < TIMES; i++) {
    	// Sequential
        int fd = open(filenames[fileindex], O_RDONLY);
        fcntl(fd, F_NOCACHE, 1);
        
        size_t size = getFileSize(fd);
        off_t blockSize = 4 * 1024;
        nBlocks = size / blockSize;
        
        char* buf = (char*)malloc(size);
        
        for(int j = 0; j < nBlocks;j++) {
            start = rdtsc();
        	read(fd, buf, blockSize);
        	end = rdtsc();
        	total += end - start;
        }
        
        close(fd);

        // Random
        fd = open(filenames[fileindex], O_RDONLY);
        fcntl(fd, F_NOCACHE, 1);
        
        size = getFileSize(fd);
        blockSize = 4 * 1024;
        nBlocks = size / blockSize;
        
        buf = (char*)malloc(size);
        
        for(int j = 0; j < nBlocks;j++) {
        	off_t k = rand() % nBlocks;
            start_1 = rdtsc();
            lseek(fd, k * blockSize, SEEK_SET );
        	read(fd, buf, blockSize);
        	end_1 = rdtsc();
        	total_1 += end_1 - start_1;
        }
        
        close(fd);

        free(buf);
    }
    double seq = total/(TIMES*nBlocks) / (2.6 * 1000);
    printf(" Sequential read : %.2lf \n", seq );
    double ran = total_1/(TIMES*nBlocks) / (2.6 * 1000);
    printf(" Random  read    : %.2lf \n", ran ); 
    
    return 0;
    
}