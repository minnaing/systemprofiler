#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include "../include/rdtsc.c"


const off_t BLOCKSIZE = 4*1024;

size_t getFileSize(int fd) {
    struct stat buf;
    fstat(fd, &buf);
    return buf.st_size;
}

double sequential_read(const char *file){
    int fd = open(file, O_SYNC);
   	fcntl(fd, F_NOCACHE, 1); 
   	size_t FILESIZE = getFileSize(fd);
   	void *buf = malloc(BLOCKSIZE);


    long start, end, total;
    total = 0;
	
    while (1) {
        start = rdtsc();
        if (read(fd, buf, BLOCKSIZE) <= 0) {
            break;
        }
        end = rdtsc();
        total += end - start;
    }
    close(fd);
    free(buf);
    double num =  FILESIZE/ BLOCKSIZE;
    return total / num / 2.6 / 1000;
}

double random_read(const char *file)
{
    int fd = open(file, O_SYNC);
    fcntl(fd, F_NOCACHE, 1);
    size_t FILESIZE = getFileSize(fd);
    void *buf = malloc(BLOCKSIZE);
    off_t num = FILESIZE / BLOCKSIZE;
   
    
    long start, end, total;
    total = 0;

    for (int i = 0; i < num; i++) {
        off_t k = rand() % num;
        start = rdtsc();
        lseek(fd, k * BLOCKSIZE, SEEK_SET); // offset
        read(fd, buf, BLOCKSIZE);
        end = rdtsc();
        total += end - start;
    }
    close(fd);
    free(buf);
    return total / (double)num / 2.6 / 1000;
}

int main(int argc, const char *argv[]) {
    int fd = open(argv[1], O_SYNC);
    size_t filesize = getFileSize(fd);
    int filesize_h = filesize / ( 1024 * 1024); 
    close(fd);

    double seq = sequential_read(argv[1]);
    double ran = random_read(argv[1]);
    printf(" File size       : %.2d MB \n", filesize_h);
    printf(" Sequential read : %.2lf \n", seq);
    printf(" Random  read    : %.2lf \n", ran); 
    return 0;
}

