#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include "../include/rdtsc.c"
//#include "../include/config.c"

#define TIMES 10

int main(){
	long readTotal, writeTotal;
	long start, end;
	double each;
	readTotal = 0;
	writeTotal = 0;
	int i, j, k;

	puts(" --- [ Benchmarking RAM Bandwidth ] --- ");

	int size = 32 * 1024 * 1024; // 32 MB
	int * array = (int *)malloc(size * sizeof(int));
	for (i = 0; i < size; i++){ array[i] = 0; }

	int temp = 0;
	int offset = size /4;
	double result = 0.0;

	for (i = 0; i < TIMES; i++){
		start = rdtsc();
		for (j = 0; j < offset; j++){
			/*
			for (k = j; k < offset; k++){
				temp = array[k];
				temp = array[k + offset];
			}
			*/
			temp = array[j];
			temp = array[j + offset];
			temp = array[j + offset + offset];
			temp = array[j + offset + offset + offset];
		}
		end = rdtsc();
		readTotal += end - start;
		//printf("%ld\n", readTotal);
		each = ( 8 * 32 * 1.0 / readTotal) * 2.6e9;
		result += each;
		printf("%f : %f\n", each, result);
	}
	result /= TIMES;
	printf(" Average READ        : %f \n", result);
/*
	each = writeTotal  * 1.0 / TIMES ;
	printf(" Time to run %d iteration  : %ld \n", TIMES, readTotal);
	printf(" Average READ        : %f \n", each); */

}
