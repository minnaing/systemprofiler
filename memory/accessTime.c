#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include "../include/rdtsc.c"
#include "../include/config.c"


double cacheAccessTime(int size, int strideLength){
	long start, end, total;
	total = 0;

	int* array = (int *)malloc(size * 1024 / 4 * sizeof(int));
	//memset((char *)array, 1, sizeof(int));

    //int stride = strideLength / 4;
    int array_size = size * 1024 / 4;
    for (int i = 0; i < array_size; i++) {
        int index = (i + (strideLength / 4)) % array_size;
        array[i] = index;
    }

    int x = 0;

	start = rdtsc();
    for (int i = 0; i < TIMES; i++) {
		    x = array[x]; //array[rand() % array_size];
	}
	end = rdtsc();

    total = end - start;
    double ans = ((total - 23) * 1.0) / TIMES - 6;
	free(array);
    printf(" %d kB size with %d stride : %f \n", size, strideLength, ans);
    return ans;
}


int test_on_size(int size, int stride){
	long int start, end, total;
	double each;
	total = 0;
	int pos;
	// Allocation and Writing
	// malloc( size_t ) // require byte
	int * array = (int *)malloc(size * 1024/ 4 * sizeof(int));
	int array_size  = size * 1024 / 4;
	for (int i = 0; i < array_size; i++){
		int val = (i + (stride / 4)) % array_size;
		array[i] = val;
	}

	// Random Access
	pos = 0; // rand() % size; // i % size; //

	start = rdtsc();
	for (int i = 0; i < TIMES; i++){
	int temp = array[0]; //array[rand() % size];

	}
	end = rdtsc();

	total = end - start;

	free(array);
	each = (total - 23) * 1.0 / (TIMES -6);
	//printf(" Run %d ...\n" , size);
	//printf(" Time to run %d iteration     : %ld \n", TIMES, total);
	printf(" %d kB size with %d stride : %f \n", size, stride, each);
	//printf("%d\t%f\n", size, each);
	return each;
}

int main(){
	srand(time(NULL));
	int sample_size[] = {
		4, // * 1024, // 4KB
		8, // * 1024, // 8KB
		16, // * 1024, // 16KB
		32, // * 1024, // 32KB ***
		64, //* 1024, // 64KB
		128, // * 1024, // 128KB
		256, // * 1024, // 256KB ***
		512, // * 1024, // 512KB
		1024, // * 1024, // 1024KB = 1MB
		//1536 * 1024, // 1.5 MB
		2048, // * 1024, // 2 MB
		//2560 * 1024, // 2.5 MB
		3072, // * 1024, // 3 MB ***
		4 * 1024, // * 1024, // 4 MB
		8 * 1024, // * 1024, // 8 MB
		16 * 1024, // * 1024, // 16 MB
		32 * 1024 // * 1024, // 32MB
	};
	int sample_stride[] = {
		16, 64, 128, 512, 1024, 65536, 1024 * 1024
	};
	int size_len = sizeof(sample_size)/sizeof(int);
	int stride_len = sizeof(sample_stride)/sizeof(int);
	//puts(" --- [ Benchmarking RAM Access Time ] --- ");
	//int j = 0;
	for ( int j = 0; j < stride_len; j++){
		for (int i = 0; i < size_len ; i++){
			//puts('.');
			//test_on_size(sample_size[i], sample_stride[j]);
			cacheAccessTime(sample_size[i], sample_stride[j]);
		}
	}

	return 0;
}
