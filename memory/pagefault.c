#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "../include/rdtsc.c"

// dd if=/dev/urandom of=file.txt bs=1048 count=2000000


int main() {
    int fd = open("/temp/file.txt", O_RDWR);
    
    unsigned int offset = 16 * 1024 * 1024; //16 MB
    unsigned int FILESIZE = 4 * 1024 * 1024 * 1024; // 4GB
    char* map =(char*) mmap(NULL, FILESIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    char c;
    long start, end, total;
    total = 0;

    int i;
    for (i = 0; i < 100; i++){
        start = rdtsc();
        c = map[(((i+1) * offset) % (FILESIZE - 1))];
        end = rdtsc();
        total = end - start;
    }
    
    double averageTime = (double)(total - 106) / 100 - 6;
    printf("Average Time: %lf\n", averageTime);
    munmap(map, FILESIZE);
    close(fd);
    return 0;
}
