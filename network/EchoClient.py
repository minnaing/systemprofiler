import socket, time
import config


RTT = 0
SETUP = 0
TEARDOWN = 0
TIMES = 1

for i in range(0, TIMES):
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	# Start UP
	start = time.time()
	sock.connect( config.ECHO_HOST )
	end = time.time()
	SETUP += end - start
	
	start = time.time()
	sock.send('hello')
	data = sock.recv(32)
	end = time.time()
	RTT += (end - start) /2
	
	start = time.time()
	sock.close()
	end = time.time()
	TEARDOWN += end - start
	

print ' RTT      : %s ms' % (RTT * 1000 / TIMES)
print ' Setup    : %s ms' % (SETUP * 1000 / TIMES)
print ' Teardown : %s ms' % (TEARDOWN * 1000 / TIMES)
