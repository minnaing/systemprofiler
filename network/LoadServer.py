import socket
import sys
import config

#FILE_SIZE = raw_input('File size : ')

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind( config.ECHO_HOST )
sock.listen(config.BACKLOG)
print ' Server is listening on port ', config.PORT, ' ... '

while True:
    conn, addr = sock.accept()
    print addr , ' START - ',
    #while True:
    data = conn.recv(64)
    if not data:
        break
    data = int(data)
    msg = '0' * int(data) * 1024 * 1024
    print ' sending %d MB - ' % data,  #sys.getsizeof(data)
    #conn.send(data)
    conn.sendall(msg)
    conn.close()
    print ' END !'
