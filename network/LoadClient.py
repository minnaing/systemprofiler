import socket
import time
import sys
import config

size = sys.argv[1]
size_int = int(size)
fileSize = size_int * 1024 * 1024
print ' Request   : %d MB' % size_int

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect( config.ECHO_HOST )
sock.send(size)

TOTAL = 0
msg = ''
while (sys.getsizeof(msg) < fileSize):
	start = time.time()
	msg += sock.recv(fileSize)
	end = time.time()
	TOTAL += end - start
sock.close()

result = TOTAL * 1000
msg_len = sys.getsizeof(msg)
avg_band = msg_len / result / 1000
print ' Result    : %f ms' % result
print ' Recieve   : %d MB' % (len(msg) / 1024.0 /1024.0)
print ' Bandwidth : %f MB/s' % avg_band
