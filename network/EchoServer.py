import socket
import config

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind( config.ECHO_HOST )
sock.listen(config.BACKLOG)
print ' Server is listening on port ', config.PORT, ' ... '

while True:
	conn, addr = sock.accept()
	print ' > ', addr , ' START ... ', 
	while True:
	    data = conn.recv(64)
	    if data:
	    	break
	    conn.send(data)
	conn.close()
	print ' END !'